using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used to easily communicate data regarding generation conflicts
public class Conflict
{
    public Superposition superPos;
    public int direction;

    public Conflict(Superposition pos, int i)
    {
        superPos = pos;
        direction = i;
    }
}
