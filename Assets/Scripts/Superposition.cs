using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Superposition
{
    public Vector2Int position;
    public List<WFCNode> potentialNodes;
    public WFCNode currentNode;
    public GameObject gameObject;
    public Dictionary<Superposition, List<WFCNode>> changes = new();

    public Superposition(int x, int y, List<WFCNode> nodes)
    {
        position = new Vector2Int(x, y);
        potentialNodes = new List<WFCNode>(nodes);
    }

    public Superposition(Vector2Int pos, List<WFCNode> nodes)
    {
        position = pos;
        potentialNodes = new List<WFCNode>(nodes);
    }

    public void Undo(Superposition pos)
    {
        if (changes.ContainsKey(pos))
        {
            potentialNodes.AddRange(changes[pos]);
            changes.Remove(pos);
        }
    }

    public void AddChanges(Superposition key, List<WFCNode> changedNodes)
    {
        if (!changes.ContainsKey(key))
        {
            changes.Add(key, changedNodes);
        }
        else changes[key].AddRange(changedNodes);
    }
}