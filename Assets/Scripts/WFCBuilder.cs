using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WFCBuilder : MonoBehaviour
{
    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private float timeToWait = 1f;

    //A 2D array that stores the collapsed tiles so we can reference them later
    private Superposition[,] _grid;

    //List containing all possible nodes
    public List<WFCNode> Nodes = new();

    //A list of all tile positions that need to be collapsed.
    //Algorithm will iterate until this is empty.
    [SerializeField] private List<Superposition> _toCollapse = new();

    //Array of offsets to make it easier to check neighbors
    //without using duplicate code

    private Vector2Int[] offsets = new Vector2Int[]
    {
        new Vector2Int(0,1), //Top
        new Vector2Int(0,-1), //Bottom
        new Vector2Int(1,0), //Right
        new Vector2Int(-1,0) //Left
    };

    private void Start()
    {
        _grid = new Superposition[width, height];

        SetUpGrid(_grid, width, height);

        StartCoroutine(CollapseWorld());
    }

    private IEnumerator CollapseWorld()
    {
        _toCollapse.Clear();
        //Start with the middle superposition in the grid
        _toCollapse.Add(_grid[width / 2, height / 2]);
        //Loop until every superposition has been collapsed
        while (_toCollapse.Count > 0)
        {
            int x = _toCollapse[0].position.x;
            int y = _toCollapse[0].position.y;

            Superposition currentSuperPos = _toCollapse[0];


            if (currentSuperPos.potentialNodes.Count < 1)
            {
                //shouldn't get here
                _grid[x, y].currentNode = Nodes[0];
                Debug.LogWarning("Attempted to collapse wave on " + x + ", " + y + " but found no valid nodes.");
            }
            else
            {
                //Check if this superposition has a valid node for this position

                //A list of valid nodes given the neighboring collapsed AND uncollapsed superpositions
                List<WFCNode> possibleNodes = GetPossibleNodes(currentSuperPos);
                WFCNode candidate;
                if (possibleNodes.Count > 0)
                {
                    candidate = GetRandomNode(possibleNodes);
                }
                else
                {
                    candidate = GetRandomNode(currentSuperPos.potentialNodes);
                    Conflict conflict = CheckForConflicts(candidate, currentSuperPos);
                    Reiterate(conflict,candidate);
                }
                currentSuperPos.currentNode = candidate;
            }

            //Instantiate the prefab associated with the selected node
            currentSuperPos.gameObject = Instantiate(_grid[x, y].currentNode.prefab, new Vector3(x, 0, y), Quaternion.identity);

            //Whittle down potential nodes of neighbors
            WhittleNeighbors(currentSuperPos);

            //Remove this node from _toCollapse to indicate that a node has been selected
            _toCollapse.RemoveAt(0);

            yield return new WaitForSeconds(timeToWait);
        }
    }

    private void Reiterate(Conflict conflict, WFCNode candidate)
    {
        Debug.Log("Reiterate Called");
        //For each change made to the potential nodes of the superposition that is causing the conflict:
        foreach(Superposition key in conflict.superPos.changes.Keys)
        {
            bool hasValidOption = false;
            //check if there was a valid node that was removed from potential nodes in this change
            foreach(WFCNode node in conflict.superPos.changes[key])
            {
                switch (conflict.direction)
                {
                    case 0:
                        if((candidate.top & node.bottom) != 0)
                        {
                            hasValidOption = true; 
                        }
                        break;

                    case 1:
                        if ((candidate.bottom & node.top) != 0)
                        {
                            hasValidOption = true;
                        }
                        break;

                    case 2:
                        if ((candidate.right & node.left) != 0)
                        {
                            hasValidOption = true;
                        }
                        break;

                    case 3:
                        if ((candidate.left & node.right) != 0)
                        {
                            hasValidOption = true;
                        }
                        break;
                } 
                if (hasValidOption) break;
            }
            //if valid node was found, undo the change that eliminated it
            //then queue the superposition who made the change back to be recollapsed
            if (hasValidOption)
            {
                conflict.superPos.Undo(key);
                Destroy(key.gameObject);
                key.gameObject = null;
                _toCollapse.Insert(1, key);
                break;
            }
        }
    }

    private Superposition GetNeighbor(Superposition current, int direction)
    {
        Vector2Int neighborPos = new Vector2Int(current.position.x + offsets[direction].x, current.position.y + offsets[direction].y);
        if (IsInsideGrid(neighborPos))
        {
            return _grid[neighborPos.x, neighborPos.y];
        }

        else return null;
    }

    private void WhittleNeighbors(Superposition currentPos)
    {
        for (int i = 0; i < offsets.Length; i++)
        {
            Superposition neighbor = GetNeighbor(currentPos, i);

            if (neighbor != null)
            {
                if (!neighbor.currentNode && !_toCollapse.Contains(neighbor))
                {
                    _toCollapse.Add(neighbor);
                }

                WhittleNeighbor(currentPos, neighbor, i);
            }

        }
    }

    private void WhittleNeighbor(Superposition subject, Superposition neighbor, int direction)
    {
        List<WFCNode> nodesDeleted = new();
        for (int i = neighbor.potentialNodes.Count - 1; i > -1; i--)
        {
            WFCNode candidate = neighbor.potentialNodes[i];
            switch (direction)
            {
                //Top Neighbor
                case 0:
                    if ((subject.currentNode.top & candidate.bottom) == 0)
                    {
                        neighbor.potentialNodes.Remove(candidate);
                        nodesDeleted.Add(candidate);
                    }
                    break;
                //Bottom Neighbor
                case 1:
                    if ((subject.currentNode.bottom & candidate.top) == 0)
                    {
                        neighbor.potentialNodes.Remove(candidate);
                        nodesDeleted.Add(candidate);
                    }
                    break;
                //Right Neighbor
                case 2:
                    if ((subject.currentNode.right & candidate.left) == 0)
                    {
                        neighbor.potentialNodes.Remove(candidate);
                        nodesDeleted.Add(candidate);
                    }
                    break;
                //Left Neighbor
                case 3:
                    if ((subject.currentNode.left & candidate.right) == 0)
                    {
                        neighbor.potentialNodes.Remove(candidate);
                        nodesDeleted.Add(candidate);
                    }
                    break;

            }
        }
        //Record changes that the subject did to the neighbor.
        if (nodesDeleted.Count > 0) neighbor.AddChanges(subject, nodesDeleted);
    }

    private WFCNode GetRandomNode(List<WFCNode> nodes)
    {
        if (nodes.Count > 0)
        {
            WFCNode node = nodes[Random.Range(0, nodes.Count)];
            return node;
        }
        return null;
    }

    private List<WFCNode> GetPossibleNodes(Superposition currentPos)
    {
        List<WFCNode> possibleNodes = new();
        foreach (WFCNode node in currentPos.potentialNodes)
        {
            if (CheckForConflicts(node, currentPos) == null)
            {
                possibleNodes.Add(node);
            }
        }
        return possibleNodes;
    }

    //Returns the neighbour that caused the check to fail
    private Conflict CheckForConflicts(WFCNode candidate, Superposition currentPos)
    {

        for (int i = 0; i < offsets.Length; i++)
        {
            Superposition neighbor = GetNeighbor(currentPos, i);

            if (neighbor != null)
            {
                bool isValid = false;
                foreach (WFCNode node in neighbor.potentialNodes)
                {
                    switch (i)
                    {
                        case 0:
                            if ((candidate.top & node.bottom) != 0)
                            {
                                isValid = true;
                            }
                            break;
                        case 1:
                            if ((candidate.bottom & node.top) != 0)
                            {
                                isValid = true;
                            }
                            break;
                        case 2:
                            if ((candidate.right & node.left) != 0)
                            {
                                isValid = true;
                            }
                            break;
                        case 3:
                            if ((candidate.left & node.right) != 0)
                            {
                                isValid = true;
                            }
                            break;
                    }
                    if (isValid) break;
                }
                if (!isValid) return new Conflict(neighbor,i);
            }
        }
        return null;
    }

    private bool IsInsideGrid(Vector2Int v2Int)
    {
        if (v2Int.x > -1 && v2Int.x < width && v2Int.y > -1 && v2Int.y < height)
        {
            return true;
        }
        else return false;
    }

    private void SetUpGrid(Superposition[,] grid, int width, int height)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                grid[x, y] = new Superposition(x, y, Nodes);
            }
        }
    }
}




