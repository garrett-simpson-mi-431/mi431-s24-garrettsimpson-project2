using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Flags]
public enum eConnectorType {
    grass = 1,
    road = 2,
    dirt = 4,
    river = 8,
    lakeBorder = 16,
    lake = 32}

[CreateAssetMenu(fileName = "WFCNode", menuName = "WFC Node")]
[Serializable]
public class WFCNode : ScriptableObject
{
    public GameObject prefab;
    public eConnectorType top;
    public eConnectorType bottom;
    public eConnectorType left;
    public eConnectorType right;
    [Range(0, 1)]
    public float weight = 1f;
}
